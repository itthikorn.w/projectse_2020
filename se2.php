<?php include 'user.html'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<BODY class="bg-light">
    
    <div class="row m-1">
        <div class="card border-dark row m-3" style="max-width: 18rem;">
            <li class="list-group-item text-center"><ion-icon size="small" name="contact"></ion-icon>ชื่อ: อิทธิกร วิเศษพงษ์</li>
        </div>
        <div class="card border-dark row m-3" style="max-width: 18rem;">
            <li class="list-group-item text-center"><ion-icon size="small" name="contact"></ion-icon>รหัส: 6030301187</li>
        </div>
        <div class="card border-dark row m-3" style="max-width: 18rem;">
            <li class="list-group-item text-center"><ion-icon size="small" name="briefcase"></ion-icon>สาขา: วิศวกรรมคอมพิวเตอร์</li>
        </div>
        <div class="card border-dark row m-3" style="max-width: 18rem;">
            <li class="list-group-item text-center"><ion-icon size="small" name="school"></ion-icon>ชั้นปี: 3</li>
        </div>
    </div>



    <div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div id="inam" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active">
						 <div class="container">
						 	<div class="row">
						 		<div class="col-sm-12 col-lg-4">
						 			<div class="card" style="width: 300px;margin: auto;">
						 				<img src="image/tide.png" class="card-img-top">
						 				<div class="card-body">
						 					<h4 class="card-title">เน็คไทด์</h4>
						 					<p class="card-text">เป็นเน็คไทด์ที่มีขนาดเล็กและเรียวกว่าปกติ</p>
						 					<button type="button" class="btn btn-warning">ได้รับแล้ว</button>
						 					
						 				</div>
						 				
						 			</div>
						 			
						 		</div>
						 		<div class="col-sm-12 col-lg-4">
						 			<div class="card" style="width: 300px;">
						 				<img src="image/gear.png" class="card-img-top">
						 				<div class="card-body">
						 					<h4 class="card-title">เกียร์วิศวะ</h4>
						 					<p class="card-text">จะได้รับในกิจกรรมวิ่งเกียร์</p>
						 					<button type="button" class="btn btn-warning">ยังไม่ได้รับ</button>
						 					
						 				</div>
						 				
						 			</div>
						 			
						 		</div>
						 		<div class="col-sm-12 col-lg-4">
						 			<div class="card" style="width: 300px;">
						 				<img src="image/shop.png" class="card-img-top">
						 				<div class="card-body">
						 					<h4 class="card-title">เสื้อช็อป</h4>
						 					<p class="card-text">เป็นเสื้อที่ใช้ในการทำแล็ปปฏิบัติการ ใส่แล้วดูเท่เก๋กู้ด</p>
						 					<button type="button" class="btn btn-warning">ได้รับแล้ว</button>
						 					
						 				</div>
						 				
						 			</div>
						 			
						 		</div>
						 		
						 	</div>
						 	
						 </div>

						
					</div>
					<div class="carousel-item ">
						 <div class="container">
						 	<div class="row">
						 		<div class="col-sm-12 col-lg-4">
						 			<div class="card" style="width: 300px;margin: auto;">
						 				<img src="image/tide.png" class="card-img-top">
						 				<div class="card-body">
						 					<h4 class="card-title">เน็คไทด์</h4>
						 					<p class="card-text">เป็นเน็คไทด์ที่มีขนาดเล็กและเรียวกว่าปกติ</p>
						 					<button type="button" class="btn btn-warning">ได้รับแล้ว</button>
						 					
						 				</div>
						 				
						 			</div>
						 			
						 		</div>
						 		<div class="col-sm-12 col-lg-4">
						 			<div class="card" style="width: 300px;">
						 				<img src="image/gear.png" class="card-img-top">
						 				<div class="card-body">
						 					<h4 class="card-title">เกียร์วิศวะ</h4>
						 					<p class="card-text">จะได้รับในกิจกรรมวิ่งเกียร์</p>
						 					<button type="button" class="btn btn-warning">ยังไม่ได้รับ</button>
						 					
						 				</div>
						 				
						 			</div>
						 			
						 		</div>
						 		<div class="col-sm-12 col-lg-4">
						 			<div class="card" style="width: 300px;">
						 				<img src="image/shop.png" class="card-img-top">
						 				<div class="card-body">
						 					<h4 class="card-title">เสื้อช็อป</h4>
						 					<p class="card-text">เป็นเสื้อที่ใช้ในการทำแล็ปปฏิบัติการ ใส่แล้วดูเท่เก๋กู้ด</p>
						 					<button type="button" class="btn btn-warning">ได้รับแล้ว</button>
						 					
						 				</div>
						 				
						 			</div>
						 			
						 		</div>
						 		
						 	</div>
						 	
						 </div>

						
					</div>
					
				</div>
				<a href="#inam" class="carousel-control-prev" data-slide="prev">
					<span class="carousel-control-prev-icon"></span>
				</a>
				<a href="#inam" class="carousel-control-next" data-slide="next">
					<span class="carousel-control-next-icon"></span>
				</a>
				
			</div>
			
		</div>
		
	</div>
	
</div>



    <!-- <div class="row m-5">
        <div class="mx-auto font-weight-bold"><span style='font-size:20px'>ได้รับ</span></div>
        <div class="mx-auto font-weight-bold"><span style='font-size:20px'>ได้รับ</span></div>
        <div class="mx-auto font-weight-bold"><span style='font-size:20px'>ไม่ได้รับ</span></div>
    </div>
    <div class="row m-5 ">
        <img src="image/tide.png" class=" mx-auto " width="250px" height="200px">
        <img src="image/gear.png" class=" mx-auto " width="250px" height="200px">
        <img src="image/shop.png" class=" mx-auto " width="250px" height="200px">
    </div> -->
    <br>
    <p align="center" class="mx-auto"><span style='font-size:30px'>กิจกรรมทั้งหมดของฉัน</span></p>
    <div class="col-8 text-center mx-auto">
        <table id="example" class="col-8 text-center mx-auto table table-striped ">
            <thead>
                <tr>
                    <th bgcolor="#A4EADA" scope="col">ลำดับ</th>
                    <th bgcolor="#A4EADA" scope="col">กำหนดการ</th>
                    <th bgcolor="#A4EADA" scope="col">ชื่อกิจกรรม</th>
                    <th bgcolor="#A4EADA" scope="col">รายละเอียด</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="col">1</th>
                    <th scope="col">17/09/2020</th>
                    <th scope="col">ค่ายวิศวดงตาล</th>
                    <th><img src="image/check.png" class=" mx-auto " width="25" height="25"></th>
                </tr>
                <tr>
                    <th scope="col">2</th>
                    <th scope="col">18/09/2020</th>
                    <th scope="col">ค่ายวิศวดงตาล</th>
                    <th><img src="image/check.png" class=" mx-auto " width="25" height="25"></th>
                </tr>
                <tr>
                    <th scope="col">3</th>
                    <th scope="col">19/09/2020</th>
                    <th scope="col">ค่ายวิศวดงตาล</th>
                    <th><img src="image/check.png" class=" mx-auto " width="25" height="25"></th>
                </tr>
                <tr>
                    <th scope="col">4</th>
                    <th scope="col">05/11/2020</th>
                    <th scope="col">สอนน้องร้องเพลง</th>
                    <th><img src="image/close.png" class=" mx-auto " width="25" height="25"></th>
                </tr>
                <tr>
                    <th scope="col">5</th>
                    <th scope="col">06/11/2020</th>
                    <th scope="col">สอนน้องร้องเพลง</th>
                    <th><img src="image/close.png" class=" mx-auto " width="25" height="25"></th>
                </tr>
                <tr>
                    <th scope="col">6</th>
                    <th scope="col">07/11/2020</th>
                    <th scope="col">สอนน้องร้องเพลง</th>
                    <th><img src="image/close.png" class=" mx-auto " width="25" height="25"></th>
                </tr>
                <tr>
                    <th scope="col">7</th>
                    <th scope="col">08/11/2020</th>
                    <th scope="col">สอนน้องร้องเพลง</th>
                    <th><img src="image/close.png" class=" mx-auto " width="25" height="25"></th>
                </tr>
            </tbody>
        </table>
    </div>
</BODY>

</HTML>