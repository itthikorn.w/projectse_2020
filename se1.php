<?php include 'user.html';?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<BODY class="bg-light">
  <p align="center" class="font-weight-bold"><span style='font-size:30px'>ค่ายวิศวดงตาล</span></p>
  <p align="center" class="font-weight-bold"><span style='font-size:20px'>จัดขึ้นวันที่ 15 มกราคม 2563 - วันที่ 19 มกราคม 2563 เป็นระยะเวลา 5 วัน<br>ได้ชั่วโมงกิจกรรม 80 ชั่วโมง เป็นกิจกรรมบังคับสำหรับนิสิตชั้นปีที่ 1 เพื่อเข้าไปหาเพื่อน</span></p>

  <div class="container">
    <div class="row">
      <div align="center" class="p-3 col-12 font-weight-bold" style="background-color: #61C0BF;"><span style='font-size:20px'>17/09/2020 <br> ค่ายวิศวดงตาล</span></div>
    </div>

    <div class="row-12 text-center">
      <table id="example" bgcolor="#FFFFFF" class="table table-striped">
        <thead>
          <tr>
            <th bgcolor="#A4EADA" scope="col">เวลา</th>
            <th bgcolor="#A4EADA" scope="col">กิจกรรม</th>
            <th bgcolor="#A4EADA" scope="col">หมายเหตุ</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>8.00 น.</th>
            <td>กินข้าว</td>
            <td></td>
          </tr>
          <tr>
            <th>9.00 น.</th>
            <td>รวมตัวที่อาคาร 13</td>
            <td></td>
          </tr>
          <tr>
            <th>3</th>
            <td>Larry</td>
            <td>the Bird</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</BODY>

</HTML>